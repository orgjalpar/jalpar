#!/bin/bash

print_help() {
  printf "%6s\n" "usage:"
  printf "    %-30s %-20s\n"  "run_on_file_change --help" "Display this help"
  printf "    %-30s %-20s\n"  "run_on_file_change --file <path>" "Watched File"
  printf "    %-30s %-20s\n"  "run_on_file_change --command <command>" "Command"
}

if [[ $# -eq 0 ]]; then
  print_help
  exit 1
fi

WATCHFILE_SPECIFIED="false"

while [[ $# -gt 0 ]]; do
  KEY=$1
  case $KEY in
    -h|--help)
      print_help
      exit 0
      ;;
    --file)
      shift
      if [[ $# -eq 0 ]]; then
        break
      fi
      WATCHFILE=$(realpath $1)
      WATCHFILE_SPECIFIED="true"
      shift
      ;;
    *)
      echo "Unrecognized Option $KEY"
      print_help
      exit 1
  esac
done

PRINT_HELP_AND_EXIT="false"

if [[ $WATCHFILE_SPECIFIED == "false" ]]; then
  echo "Missing File List."
  PRINT_HELP_AND_EXIT="true"
fi

if [[ $PRINT_HELP_AND_EXIT == "true" ]]; then
  print_help
  exit 1
fi


while inotifywait -e close_write $FILE;
do
  echo "Copying Everything"
  while read -r line;
  do
    cp $SOURCE $line;
  done < $DESTLIST
done
