"color scheme of the moment:
colorscheme beauty256
"colorscheme bubblegum-256-dark
"colorscheme solarized8_dark
syntax on
"highlight Normal guibg=#E0E0E0
"set guifont=Meslo\ LG\ L\ for\ Powerline\ 9
"set guifont=Droid\ Sans\ Mono\ Dotted\ for\ Powerline\ 9
GuiFont Source\ Code\ Pro\ for\ Powerline:h12
"set guifont=Inconsolata-dz\ for\ Powerline\ 9
"set guifont=Inconsolata\ for\ Powerline\ 10
"set guifont=Fira\ Code\ Retina\ 12
"set guifont=Fira\ Mono\ for\ Powerline\ 12
"set guifont=FuraCode\ Nerd\ Font\ Regular\ 12
"set guifont=FuraMonoForPowerline\ Nerd\ Font\ Regular\ 10
"set guifont=Droid\ Sans\ Mono\ Dotted\ for\ Powerline\ 11
"set guifont=Fira\ Mono\ for\ Powerline\ 11
set guioptions-=m  "remove menu bar
set guioptions-=T  "remove toolbar
set guioptions-=r  "remove right-hand scroll bar
set guioptions-=L  "remove left-hand scroll bar
set guioptions+=lrbmTLce
set guioptions-=lrbmTLce
set guioptions+=c
set visualbell t_vb=
set guicursor+=a:blinkon0
let g:airline_section_c = '' " airline#section#create_right(['filetype'])
let g:airline_section_x = airline#section#create_right(['tagbar', 'filetype'])
highlight Search guibg=RoyalBlue guifg=black
