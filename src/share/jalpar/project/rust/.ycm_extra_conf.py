def Settings(**kwargs):
    if kwargs['language'] == 'rust':
        return {
            "ls": {
                "rust-analyzer": {
                    "diagnostics": {
                        "disabled": ["incorrect-ident-case"]
                    }
                }
            }
        }
